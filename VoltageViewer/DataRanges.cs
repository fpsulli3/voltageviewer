﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoltageViewer
{
    /// <summary>
    /// Stores some simple values calculated from the voltage data.
    /// </summary>
    public class DataRanges
    {
        public double minVoltageMicroVolts;
        public double maxVoltageMicroVolts;
        public double totalVoltageRangeMicroVolts;
    }
}
