﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoltageViewer
{
    /// <summary>
    /// Represents the state of the Presenter itself. Contains 
    /// all of the voltage data, plus all of the spikes found 
    /// in that voltage data, as well as some ranges for quick 
    /// reference.
    /// </summary>
    class PresenterState
    {
        public List<DataPoint> data = null;
        public List<Spike> spikes = null;
        public DataRanges ranges = null;
    }
}
