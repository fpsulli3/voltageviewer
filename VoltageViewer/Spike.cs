﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoltageViewer
{
    /// <summary>
    /// Represents a single Spike.
    /// </summary>
    public struct Spike
    {
        private int index;
        private DataPoint dataPoint;

        public Spike(int index, DataPoint dataPoint)
        {
            this.index = index;
            this.dataPoint = dataPoint;
        }

        public int ThresholdIndex { get { return index; } }
        public DataPoint DataPoint { get { return dataPoint; } }
    }
}
