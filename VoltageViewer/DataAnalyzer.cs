﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace VoltageViewer
{
    /// <summary>
    /// A utility class for calculating some values from a set of voltage data.
    /// </summary>
    public class DataAnalyzer
    {
        /// <summary>
        /// Finds the spikes in a given set of data.
        /// </summary>
        /// <param name="data">The data in which to search for spikes.</param>
        /// <param name="minVoltage">The minimum voltage threshold</param>
        /// <param name="maxVoltage">The maximum voltage threshold</param>
        /// <returns>A list of the Spikes that were found.</returns>
        public static async Task<List<Spike>> FindSpikesAsyc(List<DataPoint> data, double minVoltage, double maxVoltage)
        {
            return await Task.Run(() => {
                List<Spike> spikes = new List<Spike>(1000);

                int i = 0;
                while (i < data.Count)
                {
                    if (data[i].V > maxVoltage || data[i].V < minVoltage)
                    {
                        spikes.Add(new Spike(i, data[i]));
                        i += 31;
                    }
                    else
                    {
                        ++i;
                    }
                }

                return spikes;
            });
        }

        /// <summary>
        /// Precalculates some very basic facts from the given set of data.
        /// </summary>
        /// <param name="data">The data from which to calculate the ranges.</param>
        /// <returns>A DataRanges containing the ranges that were calculated.</returns>
        public static async Task<DataRanges> CalculateDataRanges(List<DataPoint> data)
        {
            return await Task.Run(() =>
            {
                DataRanges dataRanges = new DataRanges();
                dataRanges.minVoltageMicroVolts = Double.MaxValue;
                dataRanges.maxVoltageMicroVolts = -Double.MaxValue;

                foreach (DataPoint datum in data)
                {
                    if (datum.V > dataRanges.maxVoltageMicroVolts)
                    {
                        dataRanges.maxVoltageMicroVolts = datum.V;
                    }

                    if (datum.V < dataRanges.minVoltageMicroVolts)
                    {
                        dataRanges.minVoltageMicroVolts = datum.V;
                    }
                }

                dataRanges.totalVoltageRangeMicroVolts = dataRanges.maxVoltageMicroVolts - dataRanges.minVoltageMicroVolts;

                return dataRanges;
            });
        }
    }
}
