﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoltageViewer
{
    /// <summary>
    /// Represents a single data point from  the voltage data. A struct 
    /// so that all the elements of a List<DataPoint> will be contiguous 
    /// in memory.
    /// </summary>
    public struct DataPoint
    {
        public Double t;
        public Double V;
    }
}
