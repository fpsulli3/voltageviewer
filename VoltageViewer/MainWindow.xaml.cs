﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace VoltageViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly VoltageViewerPresenter presenter = new VoltageViewerPresenter();
        private ViewState curViewState = null;
        private Rectangle curSpikeIndicator = new Rectangle();
        private List<Line> linePool = new List<Line>(1000);
        private Rectangle voltageBandBackground = new Rectangle();
        private Line zeroVoltageLine = new Line();
        private Line horizontalRule = new Line();
        private Line verticalRule = new Line();
        private TextBlock horizontalRuleLabel = new TextBlock();
        private TextBlock verticalRuleLabel = new TextBlock();

        public MainWindow()
        {
            InitializeComponent();

            presenter.OnError = OnError;
            curViewState = new ViewState.NoData();
            UpdateEntireView(curViewState);
        }

        private void UpdateEntireView(ViewState viewState)
        {
            UpdateTimeline(viewState);
            UpdateWaveformDisplay(viewState);
            updateVoltageBandControls(viewState);
            updateSeekControls(viewState);
        }

        private void OnError(string title, string message)
        {
            MessageBox.Show(message, title);
        }

        private void UpdateTimeline(ViewState viewState)
        {
            UpdateTimelineSpikes(viewState);
            UpdateTimlineSelectedSpikeIndicator(viewState);
        }

        private void updateVoltageBandControls(ViewState viewState)
        {
            if (viewState is ViewState.DataLoaded.WithSpikes)
            {
                minVoltageText.IsEnabled = true;
                maxVoltageText.IsEnabled = true;
                applyBandChanges.IsEnabled = true;
            }
            else
            {

                minVoltageText.IsEnabled = false;
                maxVoltageText.IsEnabled = false;
                applyBandChanges.IsEnabled = false;
            }
        }

        private void updateSeekControls(ViewState viewState)
        {
            if (viewState is ViewState.DataLoaded.WithSpikes)
            {
                ViewState.DataLoaded.WithSpikes spikeState = viewState as ViewState.DataLoaded.WithSpikes;

                bool onFirstSpike = spikeState.timeline.selectedSpikeIndex == 0;
                bool onLastSpike = spikeState.timeline.selectedSpikeIndex == spikeState.timeline.spikes.Count - 1;
                seekBackButton.IsEnabled = !onFirstSpike;
                seekForwardButton.IsEnabled = !onLastSpike;
            }
            else
            {
                seekBackButton.IsEnabled = false;
                seekForwardButton.IsEnabled = false;
            }
        }

        private void UpdateTimelineSpikes(ViewState viewState)
        {
            if (viewState is ViewState.DataLoaded.WithSpikes)
            {
                double timelineWidth = timeline.ActualWidth;
                double timelineHeight = timeline.ActualHeight;

                ViewState.DataLoaded.WithSpikes spikeState = viewState as ViewState.DataLoaded.WithSpikes;
                int curLineCount = linePool.Count;
                int requiredLines = spikeState.timeline.spikes.Count;
                int netNewLines = requiredLines - curLineCount;

                if (netNewLines > 0)
                {
                    for (int i = 0; i < netNewLines; ++i)
                    {
                        Line line = new Line();
                        line.Stroke = Brushes.DarkSeaGreen;
                        line.StrokeThickness = 2;
                        linePool.Add(line);
                        timeline.Children.Add(line);
                    }
                }
                else if (netNewLines < 0)
                {
                    for (int i = 0; i < -netNewLines; ++i)
                    {
                        timeline.Children.Remove(linePool[linePool.Count - 1 - i]);
                    }
                }

                double pixelsPerSecond = timelineWidth / spikeState.timeline.totalTimespan;

                for (int i = 0; i < spikeState.timeline.spikes.Count; ++i)
                {
                    Spike spike = spikeState.timeline.spikes[i];
                    double x = (spike.DataPoint.t - spikeState.timeline.minDisplayTimeSeconds) * pixelsPerSecond;
                    Line line = linePool[i];
                    line.X1 = line.X2 = x;
                    line.Y1 = 0;
                    line.Y2 = timelineHeight;
                }
            }
            else
            {
                foreach (Line line in linePool)
                {
                    timeline.Children.Remove(line);
                }
                linePool.Clear();
            }
        }

        private void UpdateTimlineSelectedSpikeIndicator(ViewState viewState)
        {
            if (viewState is ViewState.DataLoaded.WithSpikes)
            {
                ViewState.DataLoaded.WithSpikes state = viewState as ViewState.DataLoaded.WithSpikes;

                double timelineWidth = timeline.ActualWidth;
                double pixelsPerSecond = timelineWidth / state.timeline.totalTimespan;
                if (!timeline.Children.Contains(curSpikeIndicator))
                {
                    double timelineHeight = timeline.ActualHeight;

                    curSpikeIndicator.Fill = Brushes.Transparent;
                    curSpikeIndicator.Stroke = Brushes.OrangeRed;
                    curSpikeIndicator.StrokeThickness = 2;
                    curSpikeIndicator.Width = 6;
                    curSpikeIndicator.Height = timelineHeight;
                    timeline.Children.Add(curSpikeIndicator);
                }

                double left = state.timeline.selectedSpikeTime * pixelsPerSecond - 3;

                Canvas.SetLeft(curSpikeIndicator, left);
                Canvas.SetTop(curSpikeIndicator, 0.0);
            }
            else
            {
                timeline.Children.Remove(curSpikeIndicator);
            }
        }

        private void UpdateWaveformDisplay(ViewState viewState)
        {
            waveformDisplay.Children.Clear();
            UpdateVoltageBand(viewState);
            UpdateSpikeWaveform(viewState);
            UpdateXAxis(viewState);
            UpdateYAxis(viewState);
        }

        private void UpdateVoltageBand(ViewState viewState)
        {
            if (viewState is ViewState.DataLoaded.WithSpikes)
            {
                ViewState.DataLoaded.WithSpikes s = viewState as ViewState.DataLoaded.WithSpikes;
                SpikeWaveformViewState state = s.waveform;

                // Voltage band rectangle
                if (!waveformDisplay.Children.Contains(voltageBandBackground))
                {
                    voltageBandBackground.Fill = Brushes.PaleGoldenrod;
                    voltageBandBackground.Stroke = Brushes.Transparent;
                    voltageBandBackground.StrokeThickness = 0;
                    waveformDisplay.Children.Add(voltageBandBackground);
                }

                double voltageRangeMicroVolts = state.maxDisplayVoltageMicroVolts - state.minDisplayVoltageMicroVolts;
                double pixelsPerMicroVolt = waveformDisplay.ActualHeight / voltageRangeMicroVolts;
                double zeroVoltageHeight = waveformDisplay.ActualHeight * 0.5;
                double thresholdVoltageRangeMicroVolts = state.maxThresholdVoltageMicroVolts - state.minThresholdVoltageMicroVolts;
                voltageBandBackground.Width = waveformDisplay.ActualWidth;
                voltageBandBackground.Height = thresholdVoltageRangeMicroVolts * pixelsPerMicroVolt;
                Canvas.SetLeft(voltageBandBackground, 0.0);
                Canvas.SetTop(voltageBandBackground, zeroVoltageHeight - state.maxThresholdVoltageMicroVolts * pixelsPerMicroVolt);

                // Zero-voltage line
                if (!waveformDisplay.Children.Contains(zeroVoltageLine))
                {
                    zeroVoltageLine.Stroke = Brushes.Goldenrod;
                    zeroVoltageLine.StrokeThickness = 2;
                    waveformDisplay.Children.Add(zeroVoltageLine);
                }

                zeroVoltageLine.X1 = 0;
                zeroVoltageLine.X2 = waveformDisplay.ActualWidth;
                zeroVoltageLine.Y1 = zeroVoltageHeight;
                zeroVoltageLine.Y2 = zeroVoltageHeight;
            }
        }

        private void UpdateSpikeWaveform(ViewState viewState)
        {
            if (viewState is ViewState.DataLoaded.WithSpikes)
            {
                DrawSpikes(viewState as ViewState.DataLoaded.WithSpikes);
            }
            else if (viewState is ViewState.DataLoaded.NoSpikes)
            {
                ShowMessage("No Spikes to Display\n" +
                    "The data has successfully been loaded, but there are no spikes to display.\n" +
                    "You may see some spikes if you restrict the min/max voltage band, or you may\n" +
                    "have to load a different data file.");
            }
            else if (viewState is ViewState.DataLoaded.NotEnoughData)
            {
                ShowMessage("Not Enough Data\n" +
                    "The data has successfully been loaded, but there are fewer than two data points.\n" +
                    "You must load a different data file, with more data, in order to see spikes.");
            }
            else if (viewState is ViewState.NoData)
            {
                ShowMessage("No Data to Display\n" +
                    "Click on File -> Open Data File...");
            }
            else if (viewState is ViewState.Loading)
            {
                ShowMessage("Loading Data...");
            }
        }

        private void DrawSpikes(ViewState.DataLoaded.WithSpikes viewState)
        {
            SpikeWaveformViewState waveFormState = viewState.waveform;

            double startingTimeSeconds = waveFormState.minDisplayTimeSeconds;
            double timespanSeconds = waveFormState.totalTimespan;
            double pixelsPerSecond = waveformDisplay.ActualWidth / timespanSeconds;

            double voltageRangeMicroVolts = waveFormState.maxDisplayVoltageMicroVolts - waveFormState.minDisplayVoltageMicroVolts;
            double pixelsPerMicroVolt = waveformDisplay.ActualHeight / voltageRangeMicroVolts;
            double zeroVoltageHeight = waveformDisplay.ActualHeight * 0.5;

            Path path = new Path();
            path.Stroke = Brushes.OrangeRed;
            path.StrokeThickness = 2;

            PathGeometry geometry = new PathGeometry();
            PathFigure pathFigure = new PathFigure();
            pathFigure.StartPoint = new Point
            {
                X = 0,
                Y = zeroVoltageHeight - waveFormState.spikeWaveform[0].V * pixelsPerMicroVolt
            };

            for (int i = 0; i < waveFormState.spikeWaveform.Count; ++i)
            {
                LineSegment lineSegment = new LineSegment();
                lineSegment.Point = new Point
                {
                    X = (waveFormState.spikeWaveform[i].t - startingTimeSeconds) * pixelsPerSecond,
                    Y = zeroVoltageHeight - waveFormState.spikeWaveform[i].V * pixelsPerMicroVolt
                };
                pathFigure.Segments.Add(lineSegment);
            }

            geometry.Figures.Add(pathFigure);
            path.Data = geometry;

            waveformDisplay.Children.Add(path);
        }

        private void UpdateXAxis(ViewState viewState)
        {
            horizontalAxis.Children.Clear();
            if (viewState is ViewState.DataLoaded.WithSpikes)
            {
                ViewState.DataLoaded.WithSpikes s = viewState as ViewState.DataLoaded.WithSpikes;
                SpikeWaveformViewState state = s.waveform;

                double startingTimeSeconds = state.minDisplayTimeSeconds;
                double timespanSeconds = state.totalTimespan;
                double pixelsPerSecond = horizontalAxis.ActualWidth / timespanSeconds;
                double viewHeight = horizontalAxis.ActualHeight;

                foreach (double time in state.xAxisTimes)
                {
                    double x = pixelsPerSecond * (time - startingTimeSeconds);
                    double y = viewHeight * 0.35;
                    Line tick = new Line();
                    tick.Stroke = Brushes.DarkSlateGray;
                    tick.StrokeThickness = 1;
                    tick.X1 = tick.X2 = x;
                    tick.Y1 = 0;
                    tick.Y2 = y;
                    horizontalAxis.Children.Add(tick);

                    TextBlock label = new TextBlock();
                    label.Text = time.ToString() + "s";
                    label.FontSize = 14;
                    Canvas.SetLeft(label, x - 2);
                    Canvas.SetTop(label, y + 2);
                    horizontalAxis.Children.Add(label);
                }
            } 
        }

        private void UpdateYAxis(ViewState viewState)
        {
            verticalAxis.Children.Clear();
            if (viewState is ViewState.DataLoaded.WithSpikes)
            {
                ViewState.DataLoaded.WithSpikes s = viewState as ViewState.DataLoaded.WithSpikes;
                SpikeWaveformViewState state = s.waveform;

                double voltageRangeMicroVolts = state.maxDisplayVoltageMicroVolts - state.minDisplayVoltageMicroVolts;
                double pixelsPerMicroVolt = verticalAxis.ActualHeight / voltageRangeMicroVolts;
                double zeroVoltageHeight = verticalAxis.ActualHeight * 0.5;
                double viewWidth = verticalAxis.ActualWidth;

                foreach (double V in state.yAxisVoltagesMicroVolts)
                {
                    double y = zeroVoltageHeight - pixelsPerMicroVolt * V;
                    double x = viewWidth * 0.65;
                    Line tick = new Line();
                    tick.Stroke = Brushes.DarkSlateGray;
                    tick.StrokeThickness = 1;
                    tick.X1 = x;
                    tick.X2 = viewWidth;
                    tick.Y1 = tick.Y2 = y;
                    verticalAxis.Children.Add(tick);

                    TextBlock label = new TextBlock();
                    label.Text = V.ToString() + "uV";
                    label.FontSize = 14;
                    label.Margin = new Thickness(10, 0, 0, 0);
                    Canvas.SetLeft(label, 0);
                    Canvas.SetTop(label, y + 2);
                    verticalAxis.Children.Add(label);
                }
            }
        }

        private void ShowMessage(String messageText)
        {
            waveformDisplay.Children.Clear();
            TextBlock message = new TextBlock();
            message.Text = messageText;
            message.Margin = new Thickness(0, 60, 0, 0);
            message.TextAlignment = TextAlignment.Center;

            Binding b = new Binding("ActualWidth")
            {
                Source = waveformDisplay
            };
            BindingOperations.SetBinding(message, TextBlock.WidthProperty, b);
            waveformDisplay.Children.Add(message);
        }

        private async Task UpdateViewStateAsync(Func<ViewState, Task<ViewState>> updateFunc)
        {
            ViewState afterState;
            bool succeeded;
            do
            {
                ViewState beforeState = curViewState;
                afterState = await updateFunc(beforeState);
                succeeded = Interlocked.CompareExchange(ref curViewState, afterState, beforeState) == beforeState;
            } while (!succeeded);
        }

        private async void OpenDataFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {
                await UpdateViewStateAsync(async (previousViewState) => {
                    return await Task.Run(() => new ViewState.Loading());
                });

                UpdateEntireView(curViewState);

                await UpdateViewStateAsync(async (previousViewState) => {
                    return await presenter.LoadDataFileAsync(dialog.FileName);
                });

                UpdateEntireView(curViewState);
            }
        }

        private void Timeline_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateTimeline(curViewState);
        }

        private void WaveformDisplay_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateWaveformDisplay(curViewState);
        }

        private void MinMaxVoltageText_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            string currentText = maxVoltageText.Text;
            string suggestedChangedText = 
                currentText.Substring(0, maxVoltageText.SelectionStart) 
                + e.Text + currentText.Substring(maxVoltageText.SelectionStart 
                + maxVoltageText.SelectionLength);

            e.Handled = suggestedChangedText != "-" && !double.TryParse(suggestedChangedText, out _);
        }

        private async void SeekBackButton_Click(object sender, RoutedEventArgs e)
        {
            await UpdateViewStateAsync(async (previousViewState) => {
                return await presenter.SeekBackwardAsync(previousViewState);
            });

            ViewState state = curViewState;
            if (state is ViewState.DataLoaded.WithSpikes)
            {
                ViewState.DataLoaded.WithSpikes spikeState = state as ViewState.DataLoaded.WithSpikes;
                UpdateTimlineSelectedSpikeIndicator(state);
                UpdateWaveformDisplay(state);
                updateSeekControls(state);
            }
        }

        private async void SeekForwardButton_Click(object sender, RoutedEventArgs e)
        {
            await UpdateViewStateAsync(async (previousViewState) => {
                return await presenter.SeekForwardAsync(previousViewState);
            });

            ViewState state = curViewState;
            if (state is ViewState.DataLoaded.WithSpikes)
            {
                ViewState.DataLoaded.WithSpikes spikeState = state as ViewState.DataLoaded.WithSpikes;
                UpdateTimlineSelectedSpikeIndicator(state);
                UpdateWaveformDisplay(state);
                updateSeekControls(state);
            }
        }

        private async void ApplyBandChanges_Click(object sender, RoutedEventArgs e)
        {
            string minText = minVoltageText.Text;
            string maxText = maxVoltageText.Text;

            if (minText == "-")
            {
                minVoltageText.Text = minText = "0";
            }

            if (maxText == "-")
            {
                maxVoltageText.Text = maxText = "0";
            }

            double min = double.Parse(minText);
            double max = double.Parse(maxText);
            await UpdateViewStateAsync(async (previousViewState) => {
                return await presenter.ChangeVoltageBandThresholds(min, max, previousViewState);
            });
            UpdateEntireView(curViewState);
        }

        private void WaveformDisplay_MouseLeave(object sender, MouseEventArgs e)
        {
            waveformDisplay.Children.Remove(horizontalRule);
            waveformDisplay.Children.Remove(verticalRule);
            waveformDisplay.Children.Remove(horizontalRuleLabel);
            waveformDisplay.Children.Remove(verticalRuleLabel);
        }

        private void WaveformDisplay_MouseMove(object sender, MouseEventArgs e)
        {
            ViewState state = curViewState;
            if (state is ViewState.DataLoaded.WithSpikes)
            {
                ViewState.DataLoaded.WithSpikes spikeState = state as ViewState.DataLoaded.WithSpikes;
                if (!waveformDisplay.Children.Contains(horizontalRule))
                {
                    horizontalRule.Stroke = Brushes.DimGray;
                    horizontalRule.StrokeThickness = 1;
                    waveformDisplay.Children.Add(horizontalRule);

                    verticalRule.Stroke = Brushes.DimGray;
                    verticalRule.StrokeThickness = 1;
                    waveformDisplay.Children.Add(verticalRule);

                    horizontalRuleLabel.FontSize = 14;
                    waveformDisplay.Children.Add(horizontalRuleLabel);

                    verticalRuleLabel.FontSize = 14;
                    waveformDisplay.Children.Add(verticalRuleLabel);
                }

                double x = e.GetPosition(waveformDisplay).X;
                double displayTimeRangeSeconds = spikeState.waveform.maxDisplayTimeSeconds - spikeState.waveform.minDisplayTimeSeconds;
                double secondsPerPixel = displayTimeRangeSeconds / waveformDisplay.ActualWidth;
                double s = x * secondsPerPixel + spikeState.waveform.minDisplayTimeSeconds;
                double V = getVoltageInMicroVoltsForTime(spikeState.waveform.spikeWaveform, s);
                double displayVoltageRangeMicroVolts = spikeState.waveform.maxDisplayVoltageMicroVolts - spikeState.waveform.minDisplayVoltageMicroVolts;
                double pixelsPerMicroVolt = waveformDisplay.ActualHeight / displayVoltageRangeMicroVolts;
                double zeroVoltageHeight = waveformDisplay.ActualHeight * 0.5;
                double y = zeroVoltageHeight - pixelsPerMicroVolt * V;

                verticalRule.X1 = verticalRule.X2 = x;
                verticalRule.Y1 = y;
                verticalRule.Y2 = waveformDisplay.ActualHeight;

                verticalRuleLabel.Text = String.Format("{0:0.0000000}", s) + "s";
                Canvas.SetTop(verticalRuleLabel, waveformDisplay.ActualHeight - 30);
                Canvas.SetLeft(verticalRuleLabel, x + 10);

                horizontalRule.X1 = 0;
                horizontalRule.X2 = x;
                horizontalRule.Y1 = horizontalRule.Y2 = y;

                horizontalRuleLabel.Text = String.Format("{0:0.0}", V) + "uV";
                Canvas.SetTop(horizontalRuleLabel, y - 30);
                Canvas.SetLeft(horizontalRuleLabel, 10);
            }
            else
            {
                waveformDisplay.Children.Remove(horizontalRule);
                waveformDisplay.Children.Remove(verticalRule);
            }
        }

        private double getVoltageInMicroVoltsForTime(List<DataPoint> waveForm, double t)
        {
            if  (t < waveForm[0].t || t > waveForm[waveForm.Count - 1].t)
            {
                return -1;
            }

            DataPoint before = waveForm[0];
            DataPoint after = waveForm[1];
            foreach (DataPoint dataPoint in waveForm)
            {
                if (dataPoint.t < t)
                {
                    before = dataPoint;
                }
                else if (dataPoint.t > t)
                {
                    after = dataPoint;
                    break;
                }
            }

            return ((t - before.t) / (after.t - before.t)) * (after.V - before.V) + before.V;
        }
    }
}
