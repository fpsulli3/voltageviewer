﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoltageViewer
{
    /// <summary>
    /// A utility class for parsing the data.
    /// </summary>
    public class DataParser
    {
        /// <summary>
        /// Reads data from a StreamReader and parses it.
        /// </summary>
        /// <param name="reader">The StreamReader from which to read the data.</param>
        /// <returns>The parsed data.</returns>
        public static async Task<List<DataPoint>> ParseDataFromStreamAsync(StreamReader reader)
        {
            List<DataPoint> data = new List<DataPoint>(500000);

            string line;
            int lineNumber = -1;
            while ((line = await reader.ReadLineAsync()) != null)
            {
                ++lineNumber;
                if (line[0] == '#')
                {
                    continue;
                }

                String[] tokens = line.Split('\t');

                if (tokens.Count() != 2)
                {
                    throw new InvalidDataException(String.Format("Invalid format on line {0}. " +
                        "Each line must have 2 values separated by a tab.", lineNumber));
                }

                DataPoint dataPoint = new DataPoint();
                dataPoint.t = Double.Parse(tokens[0]);
                dataPoint.V = Double.Parse(tokens[1]);
                data.Add(dataPoint);
            }

            return data;
        } 
    }
}
