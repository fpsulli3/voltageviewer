﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoltageViewer
{
    /// <summary>
    /// Represents the state of the UI
    /// </summary>
    abstract class ViewState
    {
        /// <summary>
        /// Represents the UI when no data has been loaded.
        /// This could either happen because the user hasn't selected 
        /// any data to load, or because the data file they selected 
        /// failed to load properly.
        /// </summary>
        internal class NoData : ViewState { }

        /// <summary>
        /// Represents the UI when data is currently being loaded.
        /// It takes several seconds to read and parse the data files, 
        /// so this state allows the UI to indicate to the user that 
        /// the data is loading.
        /// </summary>
        internal class Loading : ViewState { }

        /// <summary>
        /// Represents the UI state when the data is fully loaded.
        /// There are 3 sub-classes that each handle a different situation.
        /// </summary>
        internal abstract class DataLoaded : ViewState {
            /// <summary>
            /// Data has been loaded, but the data did not have
            /// any spikes, at least according to the current 
            /// voltage min/max thresholds.
            /// </summary>
            internal class NoSpikes : DataLoaded { }

            /// <summary>
            /// Data has been loaded, but there is not enough data 
            /// to show. We would need at least 2 data points.
            /// </summary>
            internal class NotEnoughData : DataLoaded { }

            /// <summary>
            /// Data has been loaded, and there are spikes to show.
            /// </summary>
            internal class WithSpikes : DataLoaded
            {
                public SpikeWaveformViewState waveform;
                public SpikeTimelineViewState timeline;
            }
        }
    }    

    /// <summary>
    /// Used by ViewState.DataLoaded.WithSpikes to store 
    /// the necessary data to display the currently-selected 
    /// spike waveform.
    /// </summary>
    struct SpikeWaveformViewState
    {
        public List<DataPoint> spikeWaveform;
        public List<double> xAxisTimes;
        public List<double> yAxisVoltagesMicroVolts;
        public double minDisplayTimeSeconds;
        public double maxDisplayTimeSeconds;
        public double totalTimespan;
        public double minDisplayVoltageMicroVolts;
        public double maxDisplayVoltageMicroVolts;
        public double minThresholdVoltageMicroVolts;
        public double maxThresholdVoltageMicroVolts;
        public double dt;

        public SpikeWaveformViewState ShallowCopy()
        {
            return (SpikeWaveformViewState) MemberwiseClone();
        }
    }

    /// <summary>
    /// Used by ViewState.DataLoaded.WithSpikes to store
    /// the necessary data to display the timeline.
    /// </summary>
    struct SpikeTimelineViewState
    {
        public List<Spike> spikes;
        public double minDisplayTimeSeconds;
        public double maxDisplayTimeSeconds;
        public double totalTimespan;
        public double selectedSpikeTime;
        public int selectedSpikeIndex;

        public SpikeTimelineViewState ShallowCopy()
        {
            return (SpikeTimelineViewState) MemberwiseClone();
        }
    }
}
