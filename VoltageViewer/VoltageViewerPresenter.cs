﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace VoltageViewer
{
    delegate void OnError(string title, string message);
    class VoltageViewerPresenter
    {
        private static readonly int NUM_BEFORE_THRESHOLD = 14;
        private static readonly int NUM_AFTER_THRESHOLD = 30;
        private static readonly int TOTAL_WAVEFORM_COUNT = NUM_BEFORE_THRESHOLD + NUM_AFTER_THRESHOLD + 1;

        private double minThresholdVoltageMicroVolts = -75.0;
        private double maxThresholdVoltageMicroVolts = 75.0;
        
        PresenterState state = null;

        public OnError OnError { get;  set; }

        /// <summary>
        /// Opens the specified file, loads its contents, and generates the 
        /// resulting ViewState.
        /// </summary>
        /// <param name="fileName">The path of the file to load</param>
        /// <returns>A ViewState indicating what the UI should show.</returns>
        public async Task<ViewState> LoadDataFileAsync(string fileName)
        {
            try
            {
                PresenterState newState = await UpdateStateAsync(async (PresenterState previousState) => {
                    StreamReader reader = new StreamReader(fileName);
                    List<DataPoint> data = await DataParser.ParseDataFromStreamAsync(reader);
                    reader.Close();
                    List<Spike> spikes = await DataAnalyzer.FindSpikesAsyc(data, minThresholdVoltageMicroVolts, maxThresholdVoltageMicroVolts);
                    DataRanges ranges = await DataAnalyzer.CalculateDataRanges(data);
                    return new PresenterState
                    {
                        data = data,
                        spikes = spikes,
                        ranges = ranges
                    };
                });

                return CalculateLoadedState(newState.data, newState.spikes);
            }
            catch (IOException e)
            {
                OnError("File Load Error", "Failed to load file to to IO Exception");
                return new ViewState.NoData();
            }
            catch (InvalidDataException e)
            {
                OnError("File Load Error", e.Message);
                return new ViewState.NoData();
            }
            catch (FormatException e)
            {
                OnError("File Load Error", "Fail is not in the correct format");
                return new ViewState.NoData();
            }
        }

        /// <summary>
        /// Tells the Presenter that the min/max voltage thresholds have been changes. This 
        /// results in the recalculation of voltage spikes, and a new ViewState.
        /// </summary>
        /// <param name="newMinThreshold">The new minimum voltage threshold.</param>
        /// <param name="newMaxThreshold">The new maximum voltage threshold.</param>
        /// <param name="previousViewState">The previous ViewState processed by the UI.</param>
        /// <returns>A new ViewState to be displayed by the UI.</returns>
        public async Task<ViewState> ChangeVoltageBandThresholds(double newMinThreshold, double newMaxThreshold, ViewState previousViewState)
        {
            PresenterState newState = await UpdateStateAsync(async (PresenterState previousPresenterState) => {
                if (newMaxThreshold < newMinThreshold)
                {
                    OnError("Error", "Min voltage must be lower than max voltage.");
                    return previousPresenterState;
                }
                minThresholdVoltageMicroVolts = newMinThreshold;
                maxThresholdVoltageMicroVolts = newMaxThreshold;

                if (previousPresenterState != null)
                {
                    List<Spike> spikes = await DataAnalyzer.FindSpikesAsyc(previousPresenterState.data, minThresholdVoltageMicroVolts, maxThresholdVoltageMicroVolts);
                    return new PresenterState()
                    {
                        data = previousPresenterState.data,
                        spikes = spikes,
                        ranges = previousPresenterState.ranges
                    };
                }
                else
                {
                    return null;
                }
            });

            if (previousViewState is ViewState.DataLoaded)
            {
                return CalculateLoadedState(newState.data, newState.spikes);
            }
            else
            {
                return previousViewState;
            }
        }

        /// <summary>
        /// Selects the previous available Spike, chronologically from the 
        /// currently-slected Spike. 
        /// </summary>
        /// <param name="previousViewState">The ViewState last processed by the UI.</param>
        /// <returns>A new ViewState to be displayed by the UI.</returns>
        public async Task<ViewState> SeekBackwardAsync(ViewState previousViewState)
        {
            return await Task.Run(() => {
                if (previousViewState is ViewState.DataLoaded.WithSpikes)
                {
                    ViewState.DataLoaded.WithSpikes oldState = previousViewState as ViewState.DataLoaded.WithSpikes;
                    int selectedSpikeIndex = oldState.timeline.selectedSpikeIndex;
                    if (selectedSpikeIndex > 0)
                    {
                        --selectedSpikeIndex;
                        return Seek(oldState, selectedSpikeIndex);
                    }
                }

                return previousViewState;
            });
        }

        /// <summary>
        /// Selects the next available Spike, chronologically from the 
        /// currently-slected Spike. 
        /// </summary>
        /// <param name="previousViewState">The ViewState last processed by the UI.</param>
        /// <returns>A new ViewState to be displayed by the UI.</returns>
        public async Task<ViewState> SeekForwardAsync(ViewState previousViewState)
        {
            return await Task.Run(() => {
                if (previousViewState is ViewState.DataLoaded.WithSpikes)
                {
                    ViewState.DataLoaded.WithSpikes oldState = previousViewState as ViewState.DataLoaded.WithSpikes;
                    int selectedSpikeIndex = oldState.timeline.selectedSpikeIndex;
                    if (selectedSpikeIndex < state.spikes.Count - 1)
                    {
                        ++selectedSpikeIndex;
                        return Seek(oldState, selectedSpikeIndex);
                    }
                }

                return previousViewState;
            });
        }

        /// <summary>
        /// Updates the PresenterState in a thread-safe way without locking, by 
        /// using an atomic compare-and-swap.
        /// </summary>
        /// <param name="updateFunc"></param>
        /// <returns></returns>
        private async Task<PresenterState> UpdateStateAsync(Func<PresenterState, Task<PresenterState>> updateFunc)
        {
            PresenterState afterState;
            bool succeeded;
            do
            {
                PresenterState beforeState = state;
                afterState = await updateFunc(beforeState);
                succeeded = Interlocked.CompareExchange(ref state, afterState, beforeState) == beforeState;
            } while (!succeeded);

            return afterState;
        }

        /// <summary>
        /// Given a set of data and its associated spikes, will create 
        /// the correct ViewState.DataLoaded to be sent to the UI.
        /// </summary>
        /// <param name="data">The currently-loaded data.</param>
        /// <param name="spikes">The spikes found in the currently-loaded data.</param>
        /// <returns>The correct ViewState to be sent to the UI.</returns>
        private ViewState.DataLoaded CalculateLoadedState(List<DataPoint> data, List<Spike> spikes)
        {
            if (data.Count < 2)
            {
                return new ViewState.DataLoaded.NotEnoughData();
            }
            if (spikes.Count == 0)
            {
                return new ViewState.DataLoaded.NoSpikes();
            }
            else
            {
                ViewState.DataLoaded.WithSpikes loadedState = new ViewState.DataLoaded.WithSpikes
                {
                    timeline = CalculateTimelineViewState(data, spikes, 0),
                    waveform = CalculateWaveformViewStateForSpike(data, spikes, state.ranges, 0)
                };

                return loadedState;
            }
        }

        /// <summary>
        /// Will calculate the correct ViewState for a given selected Spike.
        /// </summary>
        /// <param name="oldState">The old ViewState (must be a ViewState.DataLoaded.WithSpikes)</param>
        /// <param name="selectedSpikeIndex">The index of the spike we'd like to seek to.</param>
        /// <returns>A new ViewState that reflects the change in the selected spike.</returns>
        private ViewState Seek(ViewState.DataLoaded.WithSpikes oldState, int selectedSpikeIndex)
        {
            PresenterState presenterState = state;
            Spike selectedSpike = presenterState.spikes[selectedSpikeIndex];

            ViewState.DataLoaded.WithSpikes newState = new ViewState.DataLoaded.WithSpikes();

            newState.waveform = CalculateWaveformViewStateForSpike(
                presenterState.data, 
                presenterState.spikes, 
                presenterState.ranges, 
                selectedSpikeIndex);

            SpikeTimelineViewState newTimelineState = oldState.timeline.ShallowCopy();
            newTimelineState.selectedSpikeIndex = selectedSpikeIndex;
            newTimelineState.selectedSpikeTime = selectedSpike.DataPoint.t;

            newState.timeline = newTimelineState;

            return newState;
        }


        private static SpikeTimelineViewState CalculateTimelineViewState(List<DataPoint> data, List<Spike> spikes, int selectedSpikeIndex)
        {
            Spike selectedSpike = spikes[selectedSpikeIndex];
            SpikeTimelineViewState timeline = new SpikeTimelineViewState
            {
                spikes = spikes,
                minDisplayTimeSeconds = data[0].t,
                maxDisplayTimeSeconds = data[data.Count - 1].t,
                selectedSpikeTime = selectedSpike.DataPoint.t,
                selectedSpikeIndex = selectedSpikeIndex
            };
            timeline.totalTimespan = timeline.maxDisplayTimeSeconds - timeline.minDisplayTimeSeconds;
            return timeline;
        }

        private SpikeWaveformViewState CalculateWaveformViewStateForSpike(List<DataPoint> data, List<Spike> spikes, DataRanges ranges, int selectedSpikeIndex)
        {
            Spike selectedSpike = spikes[selectedSpikeIndex];
            SpikeWaveformViewState waveformViewState = new SpikeWaveformViewState();
            waveformViewState.spikeWaveform = CreateSpikeWaveform(data, selectedSpike);
            waveformViewState.dt = data[1].t - data[0].t;

            if (selectedSpike.ThresholdIndex >= NUM_BEFORE_THRESHOLD)
            {
                // Use the exact timestamp from the data.
                waveformViewState.minDisplayTimeSeconds = data[selectedSpike.ThresholdIndex - NUM_BEFORE_THRESHOLD].t;
            }
            else
            {
                // The min time is from before the first data point. Approximate.
                waveformViewState.minDisplayTimeSeconds = selectedSpike.DataPoint.t - (NUM_BEFORE_THRESHOLD * waveformViewState.dt);
            }

            if (selectedSpike.ThresholdIndex + NUM_AFTER_THRESHOLD < data.Count - 1)
            {
                // Use the exact timestamp from the data.
                waveformViewState.maxDisplayTimeSeconds = data[selectedSpike.ThresholdIndex + NUM_AFTER_THRESHOLD].t;
            }
            else
            {
                // The max time is after the last data point. Approximate.
                waveformViewState.maxDisplayTimeSeconds = selectedSpike.DataPoint.t + (NUM_AFTER_THRESHOLD * waveformViewState.dt);
            }

            waveformViewState.totalTimespan = waveformViewState.maxDisplayTimeSeconds - waveformViewState.minDisplayTimeSeconds;

            waveformViewState.maxDisplayVoltageMicroVolts = Math.Max(Math.Abs(ranges.maxVoltageMicroVolts), Math.Abs(ranges.minVoltageMicroVolts));
            waveformViewState.minDisplayVoltageMicroVolts = -waveformViewState.maxDisplayVoltageMicroVolts;

            waveformViewState.minThresholdVoltageMicroVolts = minThresholdVoltageMicroVolts;
            waveformViewState.maxThresholdVoltageMicroVolts = maxThresholdVoltageMicroVolts;

            waveformViewState.xAxisTimes = new List<double>();
            for (int i = 1; i < waveformViewState.spikeWaveform.Count - 1; i+=4)
            {
                DataPoint dataPoint = waveformViewState.spikeWaveform[i];
                waveformViewState.xAxisTimes.Add(dataPoint.t);
            }

            double increment = 50;
            waveformViewState.yAxisVoltagesMicroVolts = new List<double>();
            waveformViewState.yAxisVoltagesMicroVolts.Add(0.0);
            double py = increment;
            double ny = increment;
            while (py < waveformViewState.maxDisplayVoltageMicroVolts || ny > waveformViewState.minDisplayVoltageMicroVolts)
            {
                if (py < waveformViewState.maxDisplayVoltageMicroVolts)
                {
                    waveformViewState.yAxisVoltagesMicroVolts.Add(py);
                    py += increment;
                }

                if (ny > waveformViewState.minDisplayVoltageMicroVolts)
                {
                    waveformViewState.yAxisVoltagesMicroVolts.Add(ny);
                    ny -= increment;
                }
            }

            return waveformViewState;
        }

        private List<DataPoint> CreateSpikeWaveform(List<DataPoint> data, Spike selectedSpike)
        {
            int firstWaveFormIndex = selectedSpike.ThresholdIndex - NUM_BEFORE_THRESHOLD;
            int totalWaveFormCount = TOTAL_WAVEFORM_COUNT;

            if (firstWaveFormIndex < 0)
            {
                firstWaveFormIndex = 0;
            }

            if (firstWaveFormIndex + totalWaveFormCount > data.Count)
            {
                totalWaveFormCount = data.Count - firstWaveFormIndex;
            }

            return data.GetRange(firstWaveFormIndex, totalWaveFormCount);
        }
    }
}
