﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;

namespace VoltageViewer.Tests
{
    [TestClass()]
    public class DataParserTests
    {
        [TestMethod()]
        public async Task ParseDataFromStreamAsyncTest()
        {
            // Given a StreamReader attached to a Stream 
            // containing correctly-formatted data
            string correctlyFormattedData =
            @"9.478032	-3.742379
9.478065	-6.906543
9.478098	-3.764158
9.478132	-9.620876
9.478165	-4.596392
9.478198	0.772077
9.478232	-1.075743";

            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(correctlyFormattedData);
            writer.Flush();
            stream.Position = 0;
            StreamReader reader = new StreamReader(stream);

            // When I call DataParser.ParseDataFromStreamAsync, 
            // passing in the aforementioned stream
            List<DataPoint> data = await DataParser.ParseDataFromStreamAsync(reader);

            // Then data should have the expected number of 
            // elements, and the first element should contain 
            // the expected values.
            Assert.AreEqual(7, data.Count);
            Assert.AreEqual(9.478032, data[0].t, 0.000001);
            Assert.AreEqual(-3.742379, data[0].V, 0.000001);
        }

        [TestMethod()]
        [ExpectedException(typeof(InvalidDataException))]
        public async Task ParseDataFromStreamAsyncTestInvalidFormat()
        {
            // Given a StreamReader attached to a Stream 
            // containing data with an invalid format
            string incorrectlyFormattedData =
            @"9.478032	-3.742379
9.478065	-6.906543
9.478098	-3.764158
9.478132	-9.620876
9.478165	-4.596392   9.478165	-4.596392
9.478198	0.772077
9.478232	-1.075743";

        MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(incorrectlyFormattedData);
            writer.Flush();
            stream.Position = 0;
            StreamReader reader = new StreamReader(stream);

            // When I call DataParser.ParseDataFromStreamAsync, 
            // passing in the aforementioned stream
            await DataParser.ParseDataFromStreamAsync(reader);

            // Then an InvalidDataException should be thrown
        }

        [TestMethod()]
        public async Task ParseDataFromStreamAsyncTestIgnoresComments()
        {
        // Given a StreamReader attached to a Stream 
        // containing correctly-formatted data (with comments)
        string correctlyFormattedDataWithComments =
        @"#9.478032	-3.742379
9.478065	-6.906543
9.478098	-3.764158
9.478132	-9.620876
#9.478165	-4.596392
9.478198	0.772077
#9.478232	-1.075743";

        MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(correctlyFormattedDataWithComments);
            writer.Flush();
            stream.Position = 0;
            StreamReader reader = new StreamReader(stream);

            // When I call DataParser.ParseDataFromStreamAsync, 
            // passing in the aforementioned stream
            List<DataPoint> data = await DataParser.ParseDataFromStreamAsync(reader);

            // Then data should have the expected number of 
            // elements, and the first element should contain 
            // the expected values.
            Assert.AreEqual(4, data.Count);
            Assert.AreEqual(9.478065, data[0].t, 0.000001);
            Assert.AreEqual(-6.906543, data[0].V, 0.000001);
        }
    }
}