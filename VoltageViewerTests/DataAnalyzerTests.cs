﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace VoltageViewer.Tests
{
    [TestClass()]
    public class DataAnalyzerTests
    {
        [TestMethod()]
        public async Task FindSpikesAsycTest()
        {
            // Given a set of data that contains 3 spikes (assuming 
            // a min threshold of -50uV and a max threshold of 50uv)
            List<DataPoint> data = new List<DataPoint>(300);
            double dt = 0.000025;
            for (int i = 0; i < 300; ++i)
            {
                data.Add(new DataPoint()
                {
                    t = i * dt,
                    V = 0.0
                });
            }
            data[50] = new DataPoint()
            {
                t = data[50].t,
                V = 100.0
            };
            data[150] = new DataPoint()
            {
                t = data[50].t,
                V = 100.0
            };
            data[250] = new DataPoint()
            {
                t = data[50].t,
                V = 100.0
            };

            // When I call DataAnalyzer.FindSpikesAsyc, 
            // passing in the aforementioned data
            List<Spike> spikes = await DataAnalyzer.FindSpikesAsyc(data, -50, 50);

            // Then the resulting list of Spikes should have 3 elements in it.
            Assert.AreEqual(3, spikes.Count);
        }

        [TestMethod()]
        public async Task FindSpikesAsycTestTooCloseTogether()
        {
            // Given a set of data that contains 3 spikes (assuming 
            // a min threshold of -50uV and a max threshold of 50uv),
            // but two of the spikes are less than 30 data points apart
            List<DataPoint> data = new List<DataPoint>(300);
            double dt = 0.000025;
            for (int i = 0; i < 300; ++i)
            {
                data.Add(new DataPoint()
                {
                    t = i * dt,
                    V = 0.0
                });
            }
            data[50] = new DataPoint()
            {
                t = data[50].t,
                V = 100.0
            };
            data[55] = new DataPoint()
            {
                t = data[50].t,
                V = 100.0
            };
            data[250] = new DataPoint()
            {
                t = data[50].t,
                V = 100.0
            };

            // When I call DataAnalyzer.FindSpikesAsyc, 
            // passing in the aforementioned data
            List<Spike> spikes = await DataAnalyzer.FindSpikesAsyc(data, -50, 50);

            // Then the resulting list of Spikes should only have 2 elements in it
            Assert.AreEqual(2, spikes.Count);
        }

        [TestMethod()]
        public async Task CalculateDataRangesTest()
        {
            // Given a set of data where the highest voltage 
            // is 50uV and the lowest voltage is -100uV
            List<DataPoint> data = new List<DataPoint>();
            double dt = 0.000025;
            data.Add(new DataPoint()
            {
                t = 0,
                V = 50.0
            });
            data.Add(new DataPoint()
            {
                t = dt,
                V = -100.0
            });

            // When I call DataAnalyzer.CalculateDataRanges,
            // passing in the aforementioned data
            DataRanges ranges = await DataAnalyzer.CalculateDataRanges(data);

            // Then the resulting DataRanges object should show
            // a minVoltageMicroVolts of -100, a minVoltageMicroVolts of 50, 
            // and a totalVoltageRangeMicroVolts of 150
            Assert.AreEqual(-100.0, ranges.minVoltageMicroVolts);
            Assert.AreEqual(50.0, ranges.maxVoltageMicroVolts);
            Assert.AreEqual(150.0, ranges.totalVoltageRangeMicroVolts);
        }
    }
}